# Ambiente Local

Neste repositório temos o arquivo `docker-compose` contendo os dados necessários para a criação do ambiente local de forma automatizada. Quando executado, teremos uma instância de MongoDB disponível na porta 27017, assim como o Backend da aplicação executando na porta 3000 e o Frontend na porta 8080.

## Docker-compose - Comandos

Para execução, digite o comando:

```bash
docker-compose up -d
```

## Acesso

Para acessar a aplicação, copie e cole o endereço no Browser:

```bash
http://localhost:8080/
```